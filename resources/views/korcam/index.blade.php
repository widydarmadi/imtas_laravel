@extends('layout.index')




@section('content')


<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Korcam</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Korcam</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">View</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <a href="{{route('app.korcam.add')}}" class="btn btn-info">Tambah Baru</a>
              <p></p>
	            <div class="table-responsive">
	              <table id="datatable" class="table table-bordered table-striped">
	                <thead>
	                <tr>
	                  <th>No</th>
                    <th>Nama Cabang</th>
                    <th>Nama Korcam</th>
	                  <th>Aktif</th>
                    <th>Kode</th>
	                  <th>Created at</th>
	                  <th>#</th>
	                </tr>
	                </thead>
	                <tbody>
	                
	              </table>
	            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection

@section('js')
<script>
      $(document).ready( function () {
        
        $('#datatable').DataTable({
            processing: true,
            serverside: true,
            pageLength: 50,
            ajax: {
                url: '{{ route('app.korcam.datatable') }}',
                method: 'POST'
            }
        });
    });


      $('#datatable').on('click', '.delete', function(){ 
            var button_id = $(this).data("id");
            swal.fire({
                    title: "Konfirmasi",
                    text: "Anda yakin menghapus data ?",
                    icon: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Oke",
                    cancelButtonText: "Batal",
                    reverseButtons: !0
            }).then(function (e) {

                if(e.value){ 
                    $.ajax({  
                        url:"{{ route('app.korcam.delete') }}",  
                        method:"post",  
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data:{id:button_id},  
                        success:function(data)  
                        {
                            if(data.status == true){
                                swal.fire({
                                    title: "Deleted!",
                                    text: data.message,
                                    icon: "success"
                                }).then(function() {
                                    window.location = data.redirect;
                                });
                            }else{
                                swal.fire("Telah terjadi kesalahan pada sistem", data.message, "error");
                            }
                        },
                        error: function(data){
                            swal.fire("Telah terjadi kesalahan pada sistem", data.message, "error");
                        }  
                    });
                }else{

                    swal.fire("Telah terjadi kesalahan pada sistem", 'parameter invalid !', "error");
                }

             })
        });

</script>
@endsection