@extends('layout.index')




@section('content')


<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Home</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
          
          <div class="card">
            <div class="card-header bg-primary">
              <h3 class="card-title">Rekap</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">

					<table class="table table-bordered table-sm table-striped">
						<thead>
								<tr class="">
								  <th class="text-center">No</th>
								  <th class="text-center">Korcam</th>
								  <th class="text-center">TPQ</th>
								  <th class="text-center">Santri</th>
								  <th class="text-center">Infaq</th>
								  <th class="text-center">Menu</th>
								</tr>
							</thead>
							<tbody>
								@php $i = 1; @endphp
								@foreach($data->t_peserta as $row)
									<tr>
										<td class="text-center">{{$i++}}</td>
										<td>{{$row->m_korcam->nama_korcam}}</td>
										<td class="text-center">25</td>
										<td class="text-center">344</td>
										<td class="text-right">24.080.000</td>
										<td class="text-center">
											<a data-toggle="tooltip" data-placement="top" title="edit" href="{{route('app.transaksi.pendaftaran.edit', ['korcam' => $row->id_korcam])}}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
											<a data-toggle="tooltip" data-placement="top" title="kwitansi" href="{{route('app.transaksi.pendaftaran.kwitansi')}}" class="btn btn-xs btn-primary" data-toggle="tooltip" data-original-title="Kwitansi"><i class="fa fa-credit-card"></i></a>
										</td>
									</tr>
								@endforeach
							</tbody>
							<thead>
								<tr>
								  <th class="text-center" colspan="2">Jumlah</th>
								  <th class="text-center">111</th>
								  <th class="text-center">1.313</th>
								  <th class="text-right">91.910.000</th>
								  <th class=""></th>
								</tr>
							</thead>
						</table>

				</div>
                </div>
               
            </div>
            <!-- /.card-body -->
          </div>
         



        <div class="col-md-8">
          
          <div class="card">
            <div class="card-header bg-primary">
              <h3 class="card-title">Rekap</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">

                	<a href="{{ route('app.transaksi.pendaftaran.index') }}" class="btn btn-{{\Route::currentRouteName() == 'app.transaksi.pendaftaran.index' ? 'primary' : 'default'}} mb-3"><i class="fas fa-list"></i> Rekap Korcam</a>&nbsp;&nbsp;
                	<a href="{{ route('app.transaksi.pendaftaran.rekap_tpq') }}" class="btn btn-{{\Route::currentRouteName() == 'app.transaksi.pendaftaran.rekap_tpq' ? 'primary' : 'default'}} mb-3"><i class="fas fa-list"></i> Rekap TPQ</a>
                	<p></p><br>
                	<table class="table table-bordered table-sm table-striped">
						<thead>
							<tr>
							  <th>No</th>
							  <th>Induk</th>
							  <th>Korcam</th>
							  <th>Induk</th>
							  <th>TPQ</th>
							  <th>Alamat</th>
							  <th>Kepala</th>
							  <th>Santri</th>
							  <th>Peserta</th>
							  <th>%</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1; @endphp

							@foreach($data->t_peserta as $row)

								@foreach($row->t_tpq as $tpq)
								<tr>
									<td class="text-center">{{$i++}}</td>
									<td>{{$row->m_korcam->kode}}</td>
									<td>{{$row->m_korcam->nama_korcam}}</td>
									<td>{{$tpq->induk_tpq}}</td>
									<td>{{$tpq->nama_tpq}}</td>
									<td>{{$tpq->alamat_tpq}}</td>
									<td>{{$tpq->kepala_tpq}}</td>
									<td>{{$tpq->jumlah_santri}}</td>
									<td>{{count($tpq->t_santri)}}</td>
									<td>{{(count($tpq->t_santri) / 100)*$tpq->jumlah_santri}}</td>
								</tr>
								@endforeach

							@endforeach
						</tbody>
					</table>

                </div>
            </div>
          </div>
        </div>


  </section>
    <!-- /.content -->

				@endsection
