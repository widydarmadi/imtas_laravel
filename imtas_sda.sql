/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : imtas_sda

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 04/03/2021 16:04:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for file_sosialisasi
-- ----------------------------
DROP TABLE IF EXISTS `file_sosialisasi`;
CREATE TABLE `file_sosialisasi`  (
  `id` int(11) NOT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `judul` varchar(700) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `untuk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of file_sosialisasi
-- ----------------------------
INSERT INTO `file_sosialisasi` VALUES (1, NULL, '1', '2020-08-10 13:01:16', 'Surat Pemberitahuan Imtas (2 lembar)', 'Lembaga', 'Dicetak');
INSERT INTO `file_sosialisasi` VALUES (2, NULL, '1', '2020-08-10 13:01:52', 'Formulir Pendaftaran Imtas', 'Lembaga', 'Dicetak');
INSERT INTO `file_sosialisasi` VALUES (3, NULL, '1', '2020-08-10 13:01:53', 'Rekap Lembaga - Data Prestasi Santri', 'Lembaga', 'Dicetak');
INSERT INTO `file_sosialisasi` VALUES (4, NULL, '1', '2020-08-10 13:01:57', 'Rekap Korcam - Data Lembaga Imtas', 'Korcam', 'Dicetak');
INSERT INTO `file_sosialisasi` VALUES (5, NULL, '1', '2020-08-10 13:02:06', ' 	Data Peserta Imtas - Korcam ', 'Korcam', 'File diisi dan disetorkan kembali ketika mendaftar');

-- ----------------------------
-- Table structure for m_agenda_kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `m_agenda_kegiatan`;
CREATE TABLE `m_agenda_kegiatan`  (
  `id` int(11) NOT NULL,
  `nama_agenda` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `link` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_agenda_kegiatan
-- ----------------------------
INSERT INTO `m_agenda_kegiatan` VALUES (1, 'Sosialisasi Kegiatan', '1', '2020-08-10 10:55:10', 'sosialisasi_kegiatan', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (2, 'Pendaftaran', '1', '2020-08-10 10:55:13', 'pendaftaran', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (3, 'Validasi data peserta', '1', '2020-08-10 10:55:13', 'validasi_data_peserta', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (4, 'Pembuatan Jadwal', '1', '2020-08-10 10:55:14', 'pembuatan_jadwal', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (5, 'Pembuatan Kartu Peserta', '1', '2020-08-10 10:55:14', 'pembuatan_kartu_peserta', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (6, 'Teknikal Meeting', '1', '2020-08-10 10:55:15', 'teknikal_meeting', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (7, 'Pembuatan Buku Daftar Hadir', '1', '2020-08-10 10:55:15', 'buku_daftar_hadir', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (8, 'Buku Blanko & Rekap Nilai', '1', '2020-08-10 10:55:16', 'buku_rekap_nilai', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (9, 'Simulasi Imtas', '1', '2020-08-10 10:55:16', 'simulasi_imtas', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (10, 'Pelaksanaan IMTAS', '1', '2020-08-10 10:55:17', 'pelaksanaan_imtas', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (11, 'Evaluasi Hasil IMTAS', '1', '2020-08-10 11:00:36', 'evaluasi_hasil_imtas', NULL);
INSERT INTO `m_agenda_kegiatan` VALUES (12, 'Pembuatan LPJ, Piagam', '1', '2020-08-10 11:00:37', 'pembuatan_lpj', '2021-03-04 06:21:55');
INSERT INTO `m_agenda_kegiatan` VALUES (13, 'Khotmil Qur\'an & Imtihan', '1', '2020-08-10 11:00:38', 'data_imtihan', '2021-03-04 06:50:58');

-- ----------------------------
-- Table structure for m_batas_umur
-- ----------------------------
DROP TABLE IF EXISTS `m_batas_umur`;
CREATE TABLE `m_batas_umur`  (
  `id` int(11) NOT NULL,
  `tkq_batas_awal_tahun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tkq_batas_awal_bulan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tkq_batas_awal_hari` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tpq_batas_awal_tahun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tpq_batas_awal_bulan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tpq_batas_awal_hari` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remaja_batas_awal_tahun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remaja_batas_awal_bulan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remaja_batas_awal_hari` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `tkq_batas_akhir_tahun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tkq_batas_akhir_bulan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tkq_batas_akhir_hari` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tpq_batas_akhir_tahun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tpq_batas_akhir_bulan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tpq_batas_akhir_hari` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remaja_batas_akhir_tahun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remaja_batas_akhir_bulan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remaja_batas_akhir_hari` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_batas_umur
-- ----------------------------
INSERT INTO `m_batas_umur` VALUES (1, '0', '0', '0', '6', '0', '1', '9', '0', '1', '1', '2020-08-13 08:57:33', '6', NULL, NULL, '9', '0', '0', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for m_infaq
-- ----------------------------
DROP TABLE IF EXISTS `m_infaq`;
CREATE TABLE `m_infaq`  (
  `id` int(11) NOT NULL,
  `infaq_peserta_baru` int(255) NULL DEFAULT NULL,
  `infaq_peserta_ulang` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_infaq
-- ----------------------------
INSERT INTO `m_infaq` VALUES (1, 65000, 65000);

-- ----------------------------
-- Table structure for m_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `m_jabatan`;
CREATE TABLE `m_jabatan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_jabatan
-- ----------------------------
INSERT INTO `m_jabatan` VALUES (1, 'Ketua', '1');
INSERT INTO `m_jabatan` VALUES (2, 'Sekretaris 1', '1');
INSERT INTO `m_jabatan` VALUES (3, 'Sekretaris 2', '1');
INSERT INTO `m_jabatan` VALUES (4, 'Bendahara', '1');
INSERT INTO `m_jabatan` VALUES (5, 'Pj. Ijazah 1', '1');
INSERT INTO `m_jabatan` VALUES (6, 'Pj. Ijazah 2', '1');
INSERT INTO `m_jabatan` VALUES (7, 'Tim IT 1', '1');
INSERT INTO `m_jabatan` VALUES (8, 'Tim IT 2', '1');
INSERT INTO `m_jabatan` VALUES (9, 'Penata Ruang 1', '1');
INSERT INTO `m_jabatan` VALUES (10, 'Penata Ruang 2', '1');

-- ----------------------------
-- Table structure for m_korcab
-- ----------------------------
DROP TABLE IF EXISTS `m_korcab`;
CREATE TABLE `m_korcab`  (
  `id` int(11) NOT NULL,
  `nama_cabang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `kode` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_korcab
-- ----------------------------
INSERT INTO `m_korcab` VALUES (1, 'Sidoarjo', '1', '2020-08-11 13:42:59', '01.04', NULL);

-- ----------------------------
-- Table structure for m_korcam
-- ----------------------------
DROP TABLE IF EXISTS `m_korcam`;
CREATE TABLE `m_korcam`  (
  `id` int(11) NOT NULL,
  `nama_korcam` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_korcab` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_korcam
-- ----------------------------
INSERT INTO `m_korcam` VALUES (1, 'Waru', '1', '01.04.01', '2020-08-10 13:21:54', 1);
INSERT INTO `m_korcam` VALUES (2, 'Gedangan', '1', '01.04.02', '2020-08-10 13:21:55', 1);
INSERT INTO `m_korcam` VALUES (3, 'Buduran', '1', '01.04.03', '2020-08-10 13:21:56', 1);
INSERT INTO `m_korcam` VALUES (4, 'Sidoarjo', '1', '01.04.04', '2020-08-10 13:21:56', 1);
INSERT INTO `m_korcam` VALUES (5, 'Candi', '1', '01.04.05', '2020-08-10 13:21:56', 1);
INSERT INTO `m_korcam` VALUES (6, 'Taman', '1', '01.04.06', '2020-08-10 13:21:57', 1);
INSERT INTO `m_korcam` VALUES (7, 'Sukodono', '1', '01.04.07', '2020-08-10 13:21:57', 1);
INSERT INTO `m_korcam` VALUES (8, 'Krian', '1', '01.04.08', '2020-08-10 13:21:57', 1);
INSERT INTO `m_korcam` VALUES (9, 'Prambon', '1', '01.04.09', '2020-08-10 13:21:58', 1);
INSERT INTO `m_korcam` VALUES (10, 'Tulangan', '1', '01.04.10', '2020-08-10 13:22:00', 1);
INSERT INTO `m_korcam` VALUES (11, 'Wonoayu', '1', '01.04.11', '2020-08-10 13:22:00', 1);
INSERT INTO `m_korcam` VALUES (12, 'Krembung', '1', '01.04.12', '2020-08-10 13:22:01', 1);
INSERT INTO `m_korcam` VALUES (13, 'Tanggulangin', '1', '01.04.13', '2020-08-10 13:22:01', 1);
INSERT INTO `m_korcam` VALUES (14, 'Sedati', '1', '01.04.14', '2020-08-10 13:22:02', 1);
INSERT INTO `m_korcam` VALUES (15, 'Tarik', '1', '01.04.15', '2020-08-10 13:22:03', 1);
INSERT INTO `m_korcam` VALUES (16, 'Balongbendo', '1', '01.04.16', '2020-08-10 13:22:04', 1);
INSERT INTO `m_korcam` VALUES (17, 'Jabon', '1', '01.04.17', '2020-08-10 13:22:05', 1);

-- ----------------------------
-- Table structure for m_materi
-- ----------------------------
DROP TABLE IF EXISTS `m_materi`;
CREATE TABLE `m_materi`  (
  `id` int(11) NOT NULL,
  `nama_materi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_materi
-- ----------------------------
INSERT INTO `m_materi` VALUES (1, 'Fashohah', '1', '2020-08-11 11:15:48');
INSERT INTO `m_materi` VALUES (2, 'Tartil', '1', '2020-08-11 11:15:51');
INSERT INTO `m_materi` VALUES (3, 'Tajwid', '1', '2020-08-11 11:15:51');
INSERT INTO `m_materi` VALUES (4, 'Ghorib', '1', '2020-08-11 11:15:52');
INSERT INTO `m_materi` VALUES (5, 'Surat-Surat Pendek', '1', '2020-08-11 11:15:52');
INSERT INTO `m_materi` VALUES (6, 'Do\'a Harian', '1', '2020-08-11 11:15:53');
INSERT INTO `m_materi` VALUES (7, 'Sholat Putra', '1', '2020-08-11 11:15:53');
INSERT INTO `m_materi` VALUES (8, 'Sholat Putri', '1', '2020-08-11 11:15:54');
INSERT INTO `m_materi` VALUES (9, 'Wudlu Putra', '1', '2020-08-11 11:15:54');
INSERT INTO `m_materi` VALUES (10, 'Wudlu Putri', '1', '2020-08-11 11:15:55');

-- ----------------------------
-- Table structure for m_menu
-- ----------------------------
DROP TABLE IF EXISTS `m_menu`;
CREATE TABLE `m_menu`  (
  `id` int(11) NOT NULL,
  `nm_menu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_parent` int(255) NULL DEFAULT NULL,
  `link` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_menu
-- ----------------------------
INSERT INTO `m_menu` VALUES (1, 'Master Data', '1', '2020-08-12 12:50:10', NULL, NULL);
INSERT INTO `m_menu` VALUES (2, 'Agenda Kegiatan', '1', '2020-08-13 08:44:21', 1, 'agenda');
INSERT INTO `m_menu` VALUES (3, 'Batas Umur', '1', '2020-08-13 08:44:31', 1, 'batas_umur');
INSERT INTO `m_menu` VALUES (4, 'Infaq', '1', '2020-08-13 08:44:42', 1, 'infaq');
INSERT INTO `m_menu` VALUES (5, 'Jabatan', '1', '2020-08-13 08:44:49', 1, 'jabatan');
INSERT INTO `m_menu` VALUES (6, 'Korcab', '1', '2020-08-13 08:45:02', 1, 'korcab');
INSERT INTO `m_menu` VALUES (7, 'Korcam', '1', '2020-08-13 08:45:26', 1, 'korcam');
INSERT INTO `m_menu` VALUES (8, 'Materi Ujian', '1', '2020-08-13 08:45:43', 1, 'materi');
INSERT INTO `m_menu` VALUES (9, 'Menu', '1', '2020-08-13 08:45:52', 1, 'menu');
INSERT INTO `m_menu` VALUES (10, 'Hak Akses Menu', '1', '2020-08-13 08:46:04', 1, 'hak_akses');
INSERT INTO `m_menu` VALUES (11, 'Level Pengguna', '1', '2020-08-13 08:46:50', 1, 'level');
INSERT INTO `m_menu` VALUES (12, 'Pengguna', '1', '2020-08-13 08:47:04', 1, 'user');
INSERT INTO `m_menu` VALUES (13, 'Registrasi Periode', '1', '2020-08-13 08:47:36', 1, 'registrasi_periode');
INSERT INTO `m_menu` VALUES (14, 'Panitia', '1', '2020-08-13 08:48:23', 1, 'panitia');
INSERT INTO `m_menu` VALUES (15, 'Penguji', '1', '2020-08-13 08:48:30', 1, 'penguji');
INSERT INTO `m_menu` VALUES (16, 'Peserta', '1', '2020-08-13 08:48:36', 1, 'peserta');
INSERT INTO `m_menu` VALUES (17, 'TPQ', '1', '2020-08-13 08:48:43', 1, 'tpq');

-- ----------------------------
-- Table structure for m_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `m_menu_role`;
CREATE TABLE `m_menu_role`  (
  `id_menu` int(255) NULL DEFAULT NULL,
  `role` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for m_panitia
-- ----------------------------
DROP TABLE IF EXISTS `m_panitia`;
CREATE TABLE `m_panitia`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_panitia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_jabatan` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_panitia
-- ----------------------------
INSERT INTO `m_panitia` VALUES (2, 'A. Djunaidi Shofwan', '1', '2020-08-12 09:38:17', 1);
INSERT INTO `m_panitia` VALUES (9, 'Anas Bachtiar', '1', '2020-08-12 11:31:30', 2);
INSERT INTO `m_panitia` VALUES (10, 'Lutfi Hidayat', '1', '2020-08-12 11:31:38', 4);
INSERT INTO `m_panitia` VALUES (11, 'M. Imron', '1', '2020-08-12 11:31:48', 3);
INSERT INTO `m_panitia` VALUES (12, 'M. Zainul Lusmadi', '1', '2020-08-12 11:31:55', 5);
INSERT INTO `m_panitia` VALUES (13, 'M. Ridlo\'i', '1', '2020-08-12 11:32:04', 6);
INSERT INTO `m_panitia` VALUES (14, 'Mei Santo', '1', '2020-08-12 11:32:12', 7);
INSERT INTO `m_panitia` VALUES (15, 'M. Ali Yapi', '1', '2020-08-12 11:32:22', 8);
INSERT INTO `m_panitia` VALUES (16, 'Siti Muyassaroh', '1', '2020-08-12 11:32:30', 9);
INSERT INTO `m_panitia` VALUES (17, 'Binti Qoni\'ah', '1', '2020-08-12 11:32:41', 10);

-- ----------------------------
-- Table structure for m_penguji
-- ----------------------------
DROP TABLE IF EXISTS `m_penguji`;
CREATE TABLE `m_penguji`  (
  `id` int(11) NOT NULL,
  `nama_penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_materi` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_periode` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for m_peserta____
-- ----------------------------
DROP TABLE IF EXISTS `m_peserta____`;
CREATE TABLE `m_peserta____`  (
  `id` int(11) NOT NULL,
  `nama_peserta` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jk` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wali` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `tgl_masuk` date NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tpq` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for m_role
-- ----------------------------
DROP TABLE IF EXISTS `m_role`;
CREATE TABLE `m_role`  (
  `id` int(11) NOT NULL,
  `nama_role` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `alias` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_role
-- ----------------------------
INSERT INTO `m_role` VALUES (1, 'admin_korcab', '2020-08-13 10:18:15', 'Admin Korcab');
INSERT INTO `m_role` VALUES (2, 'admin_korcam', '2020-08-13 10:18:18', 'Admin Korcam');
INSERT INTO `m_role` VALUES (3, 'op_korcab', '2020-08-13 10:18:32', 'Operator Korcab');
INSERT INTO `m_role` VALUES (4, 'op_korcam', '2020-08-13 10:18:38', 'Operator Korcam');
INSERT INTO `m_role` VALUES (5, 'developer', '2020-08-13 10:18:50', 'Dev');

-- ----------------------------
-- Table structure for m_santri
-- ----------------------------
DROP TABLE IF EXISTS `m_santri`;
CREATE TABLE `m_santri`  (
  `id` int(11) NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wali` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `masuk_tpq` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_tpq` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for m_tpq
-- ----------------------------
DROP TABLE IF EXISTS `m_tpq`;
CREATE TABLE `m_tpq`  (
  `id` int(11) NOT NULL,
  `nama_tpq` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kepala_tpq` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `induk_tpq` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_korcam` int(255) NULL DEFAULT NULL,
  `jml_santri` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for m_user
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user`  (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_korcab` int(255) NULL DEFAULT NULL,
  `id_korcam` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_user
-- ----------------------------
INSERT INTO `m_user` VALUES (1, 'widy', 'e10adc3949ba59abbe56e057f20f883e', 'widy darmadi', '1', 'super_admin', '2020-08-10 12:59:56', 1, NULL);
INSERT INTO `m_user` VALUES (2, 'admin', '$2y$10$2t7NHCYlgaAffQPeKg8LoOut/7TZptIjQZXgTUYRWpNACYA1hvUx6', 'admin', '1', 'super_admin', '2021-03-03 14:47:00', 1, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_detail_periode
-- ----------------------------
DROP TABLE IF EXISTS `t_detail_periode`;
CREATE TABLE `t_detail_periode`  (
  `id_periode` bigint(255) NOT NULL,
  `id_agenda` int(255) NOT NULL,
  `tgl_mulai` date NULL DEFAULT NULL,
  `tgl_selesai` date NULL DEFAULT NULL,
  `urut` int(255) NULL DEFAULT NULL,
  `uraian_kerja` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_detail_periode
-- ----------------------------
INSERT INTO `t_detail_periode` VALUES (1, 13, '2020-08-04', '2020-08-20', 12, NULL, 0);
INSERT INTO `t_detail_periode` VALUES (1, 1, '2020-08-01', '2020-08-05', 1, '1. Surat Pemberitahuan\r\n2. Fromulir Pendaftaran\r\n3. Agenda Pelaksanaan', 1);
INSERT INTO `t_detail_periode` VALUES (1, 2, '2020-08-19', '2020-08-23', 2, 'Jarak dari sosialisasi ke pendaftaran sekitar 2 bulan', 2);
INSERT INTO `t_detail_periode` VALUES (1, 3, '2020-08-19', '2020-08-23', 3, 'Sekitar 1 hari kerja. \r\nHasil input dicetak kemudian dicek dengan formulir pendafaran.', 3);
INSERT INTO `t_detail_periode` VALUES (1, 4, '2020-08-19', '2020-08-23', 4, 'Sekitar 1 hari kerja.', 4);
INSERT INTO `t_detail_periode` VALUES (1, 5, '2020-08-19', '2020-08-23', 5, 'Sekitar 1 hari kerja. \r\n(1) Cetak, \r\n(2) Potong, \r\n(3) Tempel, \r\n(4) Packing', 5);
INSERT INTO `t_detail_periode` VALUES (1, 6, '2020-08-19', '2020-08-23', 6, 'Pembagian Kartu Peserta, dll', 6);
INSERT INTO `t_detail_periode` VALUES (1, 7, '2020-08-19', '2020-08-23', 7, 'Sekitar 1 hari kerja. Buku Daftar Hadir Peserta, Panitia, & Penguji', 7);
INSERT INTO `t_detail_periode` VALUES (1, 8, '2020-08-19', '2020-08-23', 8, 'Sekitar 1 hari kerja. ', 8);
INSERT INTO `t_detail_periode` VALUES (1, 9, '2020-08-19', '2020-08-23', 9, 'Persiapan Lokasi,\r\nRapat Tim Penguji,\r\nUji coba Aplikasi Imtas', 9);
INSERT INTO `t_detail_periode` VALUES (1, 10, '2020-08-19', '2020-08-23', 10, NULL, 10);
INSERT INTO `t_detail_periode` VALUES (1, 11, '2020-08-19', '2020-08-23', 11, NULL, 11);
INSERT INTO `t_detail_periode` VALUES (1, 12, '2020-08-19', '2020-08-23', 12, NULL, 12);

-- ----------------------------
-- Table structure for t_imtas
-- ----------------------------
DROP TABLE IF EXISTS `t_imtas`;
CREATE TABLE `t_imtas`  (
  `id` int(11) NOT NULL,
  `id_imtas` bigint(255) NULL DEFAULT NULL,
  `id_materi` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_periode
-- ----------------------------
DROP TABLE IF EXISTS `t_periode`;
CREATE TABLE `t_periode`  (
  `id` int(11) NOT NULL,
  `nama_periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun_periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `id_korcab` int(255) NULL DEFAULT NULL,
  `id_korcam` int(255) NULL DEFAULT NULL,
  `id_biaya` int(255) NULL DEFAULT NULL,
  `sekretariat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pj_kegiatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ketua_panitia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_batas_umur` int(255) NULL DEFAULT NULL,
  `tingkat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_periode
-- ----------------------------
INSERT INTO `t_periode` VALUES (1, 'Muharram 1431 H', '2020', '1', '2020-08-19 09:10:10', 1, NULL, 1, 'coba', 'coba', 'coba', 'coba', 'coba', NULL, 'KORCAB');

-- ----------------------------
-- Table structure for t_peserta
-- ----------------------------
DROP TABLE IF EXISTS `t_peserta`;
CREATE TABLE `t_peserta`  (
  `id` int(11) NOT NULL,
  `id_korcam` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `id_periode` int(255) NULL DEFAULT NULL,
  `id_tpq` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_peserta
-- ----------------------------
INSERT INTO `t_peserta` VALUES (1, 1, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (2, 2, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (3, 3, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (4, 4, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (5, 5, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (6, 6, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (7, 7, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (8, 8, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (9, 9, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (10, 10, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (11, 11, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (12, 12, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (13, 13, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (14, 14, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (15, 15, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (16, 16, NULL, 1, NULL);
INSERT INTO `t_peserta` VALUES (17, 17, NULL, 1, NULL);

-- ----------------------------
-- Table structure for t_santri
-- ----------------------------
DROP TABLE IF EXISTS `t_santri`;
CREATE TABLE `t_santri`  (
  `id` bigint(20) NOT NULL,
  `id_santri` bigint(255) NULL DEFAULT NULL,
  `id_periode` int(255) NULL DEFAULT NULL,
  `kategori_imtas` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'BARU, ULANG',
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin@admin', NULL, '$2y$10$DC0U4o4fuFiGFU5pwDJCReiv2oKP6zd2VEHdClttp27rUVeWLNLVi', NULL, '2021-03-02 01:37:51', '2021-03-02 01:37:51');

SET FOREIGN_KEY_CHECKS = 1;
