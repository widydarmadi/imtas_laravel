<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Agenda extends Controller
{
    public function index()
    {
		return view("agenda.index")->with([
            
        ]);
    }

    public function datatable()
    {
    	$list = \App\Models\M_agenda_kegiatan::orderBy('created_at', 'desc')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($list as $key => $value) {
    		$datas[$key][] = $i++;
    		$datas[$key][] = $value->nama_agenda;
    		$datas[$key][] = ($value->aktif=='1') ? '<span class="text-success">Aktif</span>' : '<span class="text-danger">Tidak Aktif</span>';
    		$datas[$key][] = $value->link;
    		$datas[$key][] = date('d-m-Y H:i:s',strtotime($value->created_at));
    		$datas[$key][] = '<a href="'.route('app.agenda.edit', ['id' => $value->id]).'" class="btn btn-sm btn-warning">edit</a> <a data-id="'.$value->id.'" class="btn btn-sm btn-danger delete" href="#">hapus</a>';
    	}

    	$data = ['data' => $datas];
		return response()->json($data);
    }

    public function add()
    {
		return view("agenda.add")->with([
            
        ]);
    }

    public function save(Request $request)
    {
    	$messages = [
            'nama_agenda.required' => 'harap diisi',
            'link.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_agenda' => ['required', 'string'],
            'link' => ['required', 'string'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nama_agenda' => $errors->first('nama_agenda'),
                'link' => $errors->first('link'),
            ]
            ]);
        }


        DB::beginTransaction();
        $update = new \App\Models\M_agenda_kegiatan;

    	$update->id = \App\Models\M_agenda_kegiatan::max('id')+1;
    	$update->nama_agenda = $request->nama_agenda;
    	$update->link = $request->link;
    	$update->aktif = $request->aktif;
    	try{
    		$update->save();
    		DB::commit();
    		return response()->json([
    			'status' => true,
    			'redirect' => route('app.agenda.index'),
    		]);
		}catch(\Exception $e){
	        DB::rollback();
	        return response()->json([
	            'message' => $e->getMessage(),
	            'status'  => false,
	        ]);
        }
    	
    }



    public function edit()
    {
    	if(!request()->filled('id') or !is_numeric(request('id'))){
    		abort(404);
    	}

    	$old = \App\Models\M_agenda_kegiatan::where([
    		'id' => request('id')
    	])->firstOrFail();

		return view("agenda.edit")->with([
            'old' => $old
        ]);
    }

    public function update(Request $request)
    {
    	$messages = [
            'nama_agenda.required' => 'harap diisi',
            'link.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_agenda' => ['required', 'string'],
            'link' => ['required', 'string'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nama_agenda' => $errors->first('nama_agenda'),
                'link' => $errors->first('link'),
            ]
            ]);
        }


        DB::beginTransaction();
        $update = \App\Models\M_agenda_kegiatan::where([
    		'id' => request('id')
    	])->first();

    	if($update == null)
    	{
    		$errors = $validator->errors();
    		return response()->json([
            'error' => [
                'nama_agenda' => 'Data tidak ditemukan !',
            ]
            ]);
    	}

    	$update->nama_agenda = $request->nama_agenda;
    	$update->link = $request->link;
    	$update->aktif = $request->aktif;
    	try{
    		$update->save();
    		DB::commit();
    		return response()->json([
    			'status' => true,
    			'redirect' => route('app.agenda.index'),
    		]);
		}catch(\Exception $e){
	        DB::rollback();
	        return response()->json([
	            'message' => $e->getMessage(),
	            'status'  => false,
	        ]);
        }
    	
    }

    public function delete(Request $request)
    {
    	if(!$request->filled('id')){
    		return response()->json([
	            'message' => 'parameter invalid !',
	            'status'  => false,
	        ]);
    	}

        $find = \App\Models\M_agenda_kegiatan::where([
    		'id' => $request->id
    	])->first();

    	if($find==null){
    		return response()->json([
    			'message' => 'parameter invalid !',
	            'status'  => false,
    		]);
    	}

    	DB::beginTransaction();

    	try{
    		$find->delete();
    		DB::commit();
    		return response()->json([
    			'status' => true,
    			'redirect' => route('app.agenda.index'),
    		]);
		}catch(\Exception $e){
	        DB::rollback();
	        return response()->json([
	            'message' => $e->getMessage(),
	            'status'  => false,
	        ]);
        }

    	return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }
}
