<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Korcam extends Controller
{
    public function index()
    {
		return view("korcam.index")->with([
            
        ]);
    }

    public function datatable()
    {
    	$list = \App\Models\M_korcam::with('m_korcab')->orderBy('created_at', 'desc')->get();
        dd($list);
    	$datas = [];
    	$i = 1;
    	foreach ($list as $key => $value) {
    		$datas[$key][] = $i++;
    		$datas[$key][] = $value->m_korcab->nama_cabang;
            $datas[$key][] = $value->nama_korcam;
            $datas[$key][] = ($value->aktif=='1') ? '<span class="text-success">Aktif</span>' : '<span class="text-danger">Tidak Aktif</span>';
            $datas[$key][] = $value->kode;
    		$datas[$key][] = date('d-m-Y H:i:s',strtotime($value->created_at));
    		$datas[$key][] = '<a href="'.route('app.korcab.edit', ['id' => $value->id]).'" class="btn btn-sm btn-warning">edit</a> <a data-id="'.$value->id.'" class="btn btn-sm btn-danger delete" href="#">hapus</a>';
    	}

    	$data = ['data' => $datas];
		return response()->json($data);
    }

    public function add()
    {
		return view("korcab.add")->with([
            
        ]);
    }

    public function save(Request $request)
    {
    	$messages = [
            'nama_korcam.required' => 'harap diisi',
            'kode.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_korcam' => ['required', 'string'],
            'kode' => ['required', 'string'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nama_korcam' => $errors->first('nama_korcam'),
                'kode' => $errors->first('kode'),
            ]
            ]);
        }


        DB::beginTransaction();
        $update = new \App\Models\M_korcam;

    	$update->id = \App\Models\M_korcam::max('id')+1;
    	$update->nama_korcam = $request->nama_korcam;
    	$update->kode = $request->kode;
    	$update->aktif = $request->aktif;
    	try{
    		$update->save();
    		DB::commit();
    		return response()->json([
    			'status' => true,
    			'redirect' => route('app.korcab.index'),
    		]);
		}catch(\Exception $e){
	        DB::rollback();
	        return response()->json([
	            'message' => $e->getMessage(),
	            'status'  => false,
	        ]);
        }
    	
    }



    public function edit()
    {
    	if(!request()->filled('id') or !is_numeric(request('id'))){
    		abort(404);
    	}

    	$old = \App\Models\M_korcam::where([
    		'id' => request('id')
    	])->firstOrFail();

		return view("korcab.edit")->with([
            'old' => $old
        ]);
    }

    public function update(Request $request)
    {
    	$messages = [
            'nama_korcam.required' => 'harap diisi',
            'kode.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_korcam' => ['required', 'string'],
            'kode' => ['required', 'string'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nama_korcam' => $errors->first('nama_korcam'),
                'kode' => $errors->first('kode'),
            ]
            ]);
        }


        DB::beginTransaction();
        $update = \App\Models\M_korcam::where([
    		'id' => request('id')
    	])->first();

    	if($update == null)
    	{
    		$errors = $validator->errors();
    		return response()->json([
            'error' => [
                'nama_korcam' => 'Data tidak ditemukan !',
            ]
            ]);
    	}

    	$update->nama_korcam = $request->nama_korcam;
    	$update->kode = $request->kode;
    	$update->aktif = $request->aktif;
    	try{
    		$update->save();
    		DB::commit();
    		return response()->json([
    			'status' => true,
    			'redirect' => route('app.korcab.index'),
    		]);
		}catch(\Exception $e){
	        DB::rollback();
	        return response()->json([
	            'message' => $e->getMessage(),
	            'status'  => false,
	        ]);
        }
    	
    }

    public function delete(Request $request)
    {
    	if(!$request->filled('id')){
    		return response()->json([
	            'message' => 'parameter invalid !',
	            'status'  => false,
	        ]);
    	}

        $find = \App\Models\M_korcam::where([
    		'id' => $request->id
    	])->first();

    	if($find==null){
    		return response()->json([
    			'message' => 'parameter invalid !',
	            'status'  => false,
    		]);
    	}

    	DB::beginTransaction();

    	try{
    		$find->delete();
    		DB::commit();
    		return response()->json([
    			'status' => true,
    			'message' => 'berhasil menghapus data !',
    			'redirect' => route('app.korcab.index'),
    		]);
		}catch(\Exception $e){
	        DB::rollback();
	        return response()->json([
	            'message' => $e->getMessage(),
	            'status'  => false,
	        ]);
        }

    	return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }
}
