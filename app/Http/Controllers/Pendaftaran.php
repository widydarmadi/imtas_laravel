<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pendaftaran extends Controller
{

	public function konstanta()
	{
		if(session('logged_in.id_korcab')){ // jika user login sebagai korcab

    		$tipe_user = 'id_korcab';
    		$where = [
    			'id_korcab' => session('logged_in.id_korcab')
    		];
    		$template = 'pendaftaran.korcab';
    		$template_rekap_tpq = 'pendaftaran.korcab_rekap_tpq';

    	}else if(session('logged_in.id_korcam')){ // jika user login sebagai korcam

    		$tipe_user = 'id_korcam';
    		$where = [
    			'id_korcam' => session('logged_in.id_korcam')
    		];
    		$template = 'pendaftaran.korcam';
    		$template_rekap_tpq = 'pendaftaran.korcam_rekap_tpq';

    	}

    	$data = [
    		'tipe_user' => $tipe_user,
    		'where' => $where,
    		'template' => $template,
    		'template_rekap_tpq' => $template_rekap_tpq,
    	];
    	return (object) $data;
	}



    public function index()
    {
    	$data = \App\Models\T_periode::Aktif()->with('t_peserta.m_korcam')->where($this->konstanta()->where)->first();
    	return view($this->konstanta()->template)->with([
            'data' => $data,
            'tipe_user' => $this->konstanta()->tipe_user,
        ]);
    }

    public function rekap_tpq()
    {
    	$data = \App\Models\T_periode::Aktif()
    			->with('t_peserta.t_tpq','t_peserta.m_korcam','t_peserta.t_tpq.t_santri')
    			->where($this->konstanta()->where)->first();
    	return view($this->konstanta()->template_rekap_tpq)->with([
            'data' => $data,
            'tipe_user' => $this->konstanta()->tipe_user,
        ]);
    }

    public function edit()
    {
    	if(!request()->filled('id_periode') or !request()->filled('id_korcam') or !is_numeric(request('id_korcam'))){
    		abort(404);
    	}

    	$old = \App\Models\M_agenda_kegiatan::where([
    		'id_korcam' => request('id_korcam')
    	])->firstOrFail();

		return view("agenda.edit")->with([
            'old' => $old
        ]);
    }

}
