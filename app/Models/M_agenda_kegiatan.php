<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_agenda_kegiatan extends Model
{
    protected $table = "m_agenda_kegiatan";
    protected $primaryKey = "id";
    
    // public function kategori_faq(){
    //     return $this->belongsTo('App\Models\Kategori_Faq','id_kategori_faq');
    // }

    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max($this->primaryKey());
    }
}

