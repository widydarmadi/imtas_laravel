<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_tpq extends Model
{
    protected $table = "t_tpq";
    protected $primaryKey = "id";
    
    // public function kategori_faq(){
    //     return $this->belongsTo('App\Models\Kategori_Faq','id_kategori_faq');
    // }
    public function t_santri(){
        return $this->hasMany(\App\Models\T_santri::class, 'id_tpq','id');
    }
}

