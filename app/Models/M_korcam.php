<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_korcam extends Model
{
    protected $table = "m_korcam";
    protected $primaryKey = "id";
    
    public function m_korcab(){
        return $this->belongsTo(\App\Models\Korcab::class,'id_korcab','id');
    }
}

