<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_detail_periode extends Model
{
    protected $table = "t_detail_periode";
    protected $primaryKey = "id";
    
    public function t_periode(){
        return $this->belongsTo(\App\Models\T_periode::class,'id_periode','id');
    }

    public function m_agenda_kegiatan(){
        return $this->belongsTo(\App\Models\M_agenda_kegiatan::class,'id_agenda','id');
    }
}

