<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_peserta extends Model
{
    protected $table = "t_peserta";
    protected $primaryKey = "id";
    
    public function m_korcam(){
        return $this->belongsTo(\App\Models\M_korcam::class,'id_korcam','id');
    }

    public function t_tpq(){
        return $this->hasMany(\App\Models\T_tpq::class, 'id_tpq','id');
    }
}

