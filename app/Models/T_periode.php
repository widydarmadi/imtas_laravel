<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_periode extends Model
{
    protected $table = "t_periode";
    protected $primaryKey = "id";
    
    // public function kategori_faq(){
    //     return $this->belongsTo('App\Models\Kategori_Faq','id_kategori_faq');
    // }
    public function scopeAktif($query){
        return $query->where('aktif', '1');
    }

    public function t_peserta(){
        return $this->hasMany(\App\Models\T_peserta::class, 'id_periode','id');
    }
}

