<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Login;
use App\Http\Controllers\Logout;


Route::get('/', [Home::class, 'index'])->name('index');
Route::get('/login', [Login::class, 'index'])->name('login');
Route::post('/login', [Login::class, 'cek'])->name('login.cek');

Route::group([
    'prefix' => 'app',
    'as' => 'app.',
    'namespace' => 'App',
    'middleware' => ['user'],
], function () {

	require_once(__DIR__.'/master.php');

	Route::get('/logout', [Logout::class, 'index'])->name('logout');
	Route::get('/', [Dashboard::class, 'index'])->name('index');
//==================================================================

	Route::group([
    'prefix' => 'transaksi',
    'as' => 'transaksi.',
    'namespace' => 'Transaksi',
	], function ()
	{
			

		Route::group([
		    'prefix' => 'sosialisasi_kegiatan',
		    'as' => 'sosialisasi_kegiatan.',
		    'namespace' => 'Sosialisasi_kegiatan',
		], function (){
			Route::get('/', [\App\Http\Controllers\Sosialisasi_kegiatan::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'pendaftaran',
		    'as' => 'pendaftaran.',
		    'namespace' => 'Pendaftaran',
		], function (){
			Route::get('/', [\App\Http\Controllers\Pendaftaran::class, 'index'])->name('index');
			Route::get('/rekap_tpq', [\App\Http\Controllers\Pendaftaran::class, 'rekap_tpq'])->name('rekap_tpq');
			Route::get('/edit', [\App\Http\Controllers\Pendaftaran::class, 'edit'])->name('edit');
			Route::get('/kwitansi', [\App\Http\Controllers\Pendaftaran::class, 'kwitansi'])->name('kwitansi');
		});


		Route::group([
		    'prefix' => 'validasi_data_peserta',
		    'as' => 'validasi_data_peserta.',
		    'namespace' => 'Validasi_data_peserta',
		], function (){
			Route::get('/', [\App\Http\Controllers\Validasi_data_peserta::class, 'index'])->name('index');
		});

		Route::group([
		    'prefix' => 'pembuatan_jadwal',
		    'as' => 'pembuatan_jadwal.',
		    'namespace' => 'Pembuatan_jadwal',
		], function (){
			Route::get('/', [\App\Http\Controllers\Pembuatan_jadwal::class, 'index'])->name('index');
		});

		Route::group([
		    'prefix' => 'pembuatan_kartu_peserta',
		    'as' => 'pembuatan_kartu_peserta.',
		    'namespace' => 'Pembuatan_kartu_peserta',
		], function (){
			Route::get('/', [\App\Http\Controllers\Pembuatan_kartu_peserta::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'teknikal_meeting',
		    'as' => 'teknikal_meeting.',
		    'namespace' => 'Teknikal_meeting',
		], function (){
			Route::get('/', [\App\Http\Controllers\Teknikal_meeting::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'buku_daftar_hadir',
		    'as' => 'buku_daftar_hadir.',
		    'namespace' => 'Buku_daftar_hadir',
		], function (){
			Route::get('/', [\App\Http\Controllers\Buku_daftar_hadir::class, 'index'])->name('index');
		});

		Route::group([
		    'prefix' => 'buku_rekap_nilai',
		    'as' => 'buku_rekap_nilai.',
		    'namespace' => 'Buku_rekap_nilai',
		], function (){
			Route::get('/', [\App\Http\Controllers\Buku_rekap_nilai::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'simulasi_imtas',
		    'as' => 'simulasi_imtas.',
		    'namespace' => 'Simulasi_imtas',
		], function (){
			Route::get('/', [\App\Http\Controllers\Simulasi_imtas::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'pelaksanaan_imtas',
		    'as' => 'pelaksanaan_imtas.',
		    'namespace' => 'Pelaksanaan_imtas',
		], function (){
			Route::get('/', [\App\Http\Controllers\Pelaksanaan_imtas::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'evaluasi_hasil_imtas',
		    'as' => 'evaluasi_hasil_imtas.',
		    'namespace' => 'Evaluasi_hasil_imtas',
		], function (){
			Route::get('/', [\App\Http\Controllers\Evaluasi_hasil_imtas::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'pembuatan_lpj',
		    'as' => 'pembuatan_lpj.',
		    'namespace' => 'Pembuatan_lpj',
		], function (){
			Route::get('/', [\App\Http\Controllers\Pembuatan_lpj::class, 'index'])->name('index');
		});


		Route::group([
		    'prefix' => 'data_imtihan',
		    'as' => 'data_imtihan.',
		    'namespace' => 'Data_imtihan',
		], function (){
			Route::get('/', [\App\Http\Controllers\Data_imtihan::class, 'index'])->name('index');
		});



	});

});


